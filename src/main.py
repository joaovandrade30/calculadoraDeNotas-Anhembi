from tkinter import *
import json

notasN2 = {
  "notasPossiveis": [
    10.0,
    9.5,
    9.0,
    8.5,
    8.0,
    7.5,
    7.0,
    6.5,
    6.0,
    5.5,
    5.0,
    4.5,
    4.0,
    3.5,
    3.0,
    2.5,
    2.0,
    1.5,
    1.0,
    0.5,
    0.0 
  ]
}

for x in notasN2["notasPossiveis"]:
  print(x)



def calculoNota(primeiraNota):

  segundaNota = 0

  calculoN1 = primeiraNota * 0.4
  calculoN2 = segundaNota * 0.6
  calculoMedia = calculoN1 + calculoN2

  print(calculoMedia)

  return calculoMedia


def mostrarNota():
  print("Pontos necessários: ")



""" Sistema de mostrar nota """

app = Tk()
app.title("Calculadora de média - Anhembi")
app.geometry("500x300")
app.configure(background="#C2C2C2")

titulo = Label(app, text="Calculadora Anhembi", background="green", foreground="#FFF")
titulo.place(x = 150, y = 50, width = 200, height = 20)

labelPrimeiraNota = Label(app, text = "Digite o valor da N1", background = "#000", foreground = "#FFF")
labelPrimeiraNota.place(x = 100, y = 120, width = 130, height = 20)

primeiraNota = Entry(app)
primeiraNota.place(x = 100, y = 160, width = 130, height = 20)

labelNotaAPS = Label(app, text = "Digite o valor da APS", background = "#000", foreground = "#FFF")
labelNotaAPS.place(x = 100, y = 200, width = 130, height = 20)

notaAPS = Entry(app)
notaAPS.place(x = 100, y = 240, width = 130, height = 20)

labelSegundaNota = Label(app, text = "Pontos necessários", background = "#000", foreground = "#FFF")
labelSegundaNota.place(x = 250, y = 120, width = 130, height = 20)

mostrarSegundaNota = Label(app, text = "", background = "#FFF", foreground = "#FFF")
mostrarSegundaNota.place(x = 250, y = 160, width = 130, height = 20)

botaoCalcular = Button(app, text = "Calcular", background = "green", foreground = "#FFF", command = mostrarNota)
botaoCalcular.place(x = 150, y = 275, width = 200, height = 20)

app.mainloop()